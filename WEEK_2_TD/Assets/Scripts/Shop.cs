using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void purchaseStandardTurret()
    {
        Debug.Log("Default wizard Tower selected");
        buildManager.SetTurretTobuild(buildManager.standardTurretPrefab);
    }
    public void purchaseAnotherTurret()
    {
        Debug.Log("exotic wizard tower selected");
        buildManager.SetTurretTobuild(buildManager.anotherTurretPrefab);
    }
}
