using UnityEngine;

public class TestProjectileScript : BulletBehaviour
{
    public Ethan ethan;
    public void OnTriggerEnter(Collider target)
    {
        ethan.speed -= 999f;
        FindObjectOfType<AudioManager>().Play("Turret2Projectile");
    }
}
