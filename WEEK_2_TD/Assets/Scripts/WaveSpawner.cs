using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public Transform ethanPrefab;
    public float timeBetweenWaves;
    public float countDown;
    private int waveIndex;
    public Transform spawnLocation;
    public Text waveCountdown;
    private void Update()
    {
        if(countDown <= 0f)
        {
            SpawnWave();
            countDown = timeBetweenWaves;
        }
        countDown -= Time.deltaTime;

        waveCountdown.text = Mathf.Round(countDown).ToString();
    }
    void SpawnWave()
    {
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy();
        }
        waveIndex++;
    }
    void SpawnEnemy()
    {
        Instantiate(ethanPrefab, spawnLocation.position, spawnLocation.rotation);
    }
}
