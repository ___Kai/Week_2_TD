using UnityEngine;

public class BubbleProjectileScript : BulletBehaviour
{
    private void OnTriggerEnter(Collider target)
    {
        Destroy(target.gameObject);
        FindObjectOfType<AudioManager>().Play("Turret1Projectile");
    }
}
