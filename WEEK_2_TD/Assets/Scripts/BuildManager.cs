using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    private GameObject turretToBuild;
    public static BuildManager instance;
    public GameObject standardTurretPrefab;
    public GameObject anotherTurretPrefab;
    private void Awake()
    {
        instance = this;
    }
    public GameObject getTurretToBuild()
    {
        return turretToBuild;
    }
    public void SetTurretTobuild(GameObject turret)
    {
        turretToBuild = turret;
    }

}
