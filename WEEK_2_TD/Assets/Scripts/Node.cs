using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Color hoverColour;
    private Color startColour;
    public Vector3 positionOffSet;
    private Renderer rend;
    private GameObject turret;
    BuildManager buildManager;
    private void Start()
    {
        rend = GetComponent<Renderer>();
        startColour = rend.material.color;
        buildManager = BuildManager.instance;
    }
    private void OnMouseDown()
    {
        if (buildManager.getTurretToBuild() == null)
            return;
        if(turret != null)
        {
            Debug.Log("You can't park here!");
            return;
        }
        GameObject turretToBuild = buildManager.getTurretToBuild();
        turret = (GameObject)Instantiate(turretToBuild, transform.position + positionOffSet, transform.rotation);
    }
    private void OnMouseEnter()
    {
        if (buildManager.getTurretToBuild() == null)
            return;
        rend.material.color = hoverColour;
    }
    private void OnMouseExit()
    {
        rend.material.color = startColour;
    }
}
